// REMAP jQUERY TO $
(function($){

$(window).on('load', function(){

    // ARIA /////////////////////////////////////

    function ariaAdd() {
        $('nav.main').attr('aria-label', 'main navigation').attr('role','menubar');
        
        $('nav.main ul').attr('role','menu');

        $('nav.main li').attr('role', 'menuitem');

        $("nav.main a").filter(function () { 
            return ($(this).siblings('ul').length > 0)
        }).attr('aria-haspopup', 'true').attr('aria-expanded', 'false');

        $('nav.main ul ul').attr('aria-label', function() {
            return ('sub menu for ' + $(this).siblings('a').text());
        });
    }

    ariaAdd();
    
    // Navigation ///////////////////////////////
    
    $('.nav-trigger').click(function(){
        $('nav.main > ul').slideToggle();
    });

    $('.drop-trigger').click(function(){
        $(this).next('ul').slideToggle();
    });

    $('.read-more-trigger').click(function(e){
        e.preventDefault();
        $(this).next('.read-more-slider').slideToggle();
        $(this).text(function(i, text){
          return text === "Read More" ? "Read Less" : "Read More";
        })
    });

    // Sliders //////////////////////////////////

    $('.home-ages .age-slider').owlCarousel({
        items: 1,
        URLhashListener:true,
        mouseDrag: 0,
        touchDrag: 0,
        pullDrag: 0,
        dots: 0,

    });

    $('.home-ages .age-slider').on('changed.owl.carousel',function(property){
        var current = property.item.index;
        var src = $(property.target).find(".owl-item").eq(current).find(".slide").attr('data-image');
        $('.image-contain').css("background-image","url("+ src +")");
    });

    $('.home-ages .btns .button').click(function(){
        $(this).addClass('active');
        $('.home-ages .btns .button').not(this).removeClass('active');
    });

    $('.home-promos .promo-slider').owlCarousel({
        items: 1,
        loop: 1
    });

    $('.home-testimonials .testimonial-slider').owlCarousel({
        items: 1,
        loop: 1
    });

    $('.owl-carousel').each(function() {
        $(this).find('.owl-dot').each(function(index) {
            var number = index + 1;
            $(this).attr('title', 'slide ' + number);
            $(this).attr('aria-label', 'slide ' + number);
            $(this).attr('alt', 'slide ' + number);
        });
    });

    $('g-recaptcha-response').attr('aria-label', 'google recapcha input');

    // Accordion ////////////////////////////////

    $('.accordion-info .trigger').click(function(e) {
        e.preventDefault();

        $('.accordion-info .trigger').not(this).next('.hidey').slideUp().parent().removeClass('active').find('svg').attr('data-icon','plus-circle');
        
        $(this).next('.hidey').slideToggle().parent().toggleClass('active');
        
        $(this).children('svg').attr('data-icon', function(i,style){
            return style === 'minus-circle' ? 'plus-circle' : 'minus-circle';
        });
    });
    
});

})(window.jQuery);
